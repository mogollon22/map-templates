app = {};

require([
	"esri/map",
	"dojo/domReady!"
],

function(Map) {
	app.map = new Map("map", {
		basemap: "gray",
		//UA
		center: [-110.950331, 32.231504],
		zoom: 16,

		//AZ
		//center: [-112.5511, 34.1334],
		//zoom: 7,

		//US
		//center: [-97, 37.5],
		//zoom: 5,
		attributionWidth: 0,
		logo: false,
		smartNavigation: false
	});


	//start coding here...
});


function resizeMap() {
	var center = app.map.extent.getCenter();
	app.map.resize();
	setTimeout(function(){ app.map.centerAt(center); },300);
}

$(document).ready(function () {

 var theme = "fresh";
    var width = $(window).width() - 410;
    var height = $(window).height() - 390;
    $("#tab-content").css("height", height);

    $('#splitter').jqxSplitter({
        width: '100%',
        height: '100%',
        theme: theme,
        panels: [{
            size: width,
            collapsible: false
        }, {
            size: 400
        }],
        resizable: false
    });


    $('#splitter').on('resize', function(event) {
        resizeMap();
    });
    $('#splitter').on('expanded', function(event) {
        resizeMap();
    });
    $('#splitter').on('collapsed', function(event) {
        resizeMap();
    });

    window.addEventListener('resize:end', function(event) {
        console.log("reszizing to " + $(window).width());
        var width = $(window).width() - 400;
        var height = $(window).height() - 390;
        //used if scroller is within splitter.
        //$("#tab-content").css("height", height);

        $('#splitter').jqxSplitter({
            width: '100%',
            height: '100%',
            panels: [{
                size: width,
                collapsible: false
            }, {
                size: 400
            }]
        });

        if ($(window).width() < 500 && ) {
            $('#splitter').jqxSplitter('collapse');
        }
        resizeMap();
    }, false);

});
