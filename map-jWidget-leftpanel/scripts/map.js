app = {};

require([
	"esri/map",
	"dojo/domReady!"
],

function(Map) {
	app.map = new Map("map", {
		basemap: "gray",
		//UA
		center: [-110.950331, 32.231504],
		zoom: 16,

		//AZ
		//center: [-112.5511, 34.1334],
		//zoom: 7,

		//US
		//center: [-97, 37.5],
		//zoom: 5,
		attributionWidth: 0,
		logo: false,
		smartNavigation: false
	});


	//start coding here...
});


function resizeMap() {
	var center = app.map.extent.getCenter();
	app.map.resize();
	setTimeout(function(){ app.map.centerAt(center); },300);
}

$(document).ready(function () {
	var theme = "fresh";
	$('#splitter').jqxSplitter({ width: '100%', height: '95%', theme: theme, panels: [{ size: '20%', max: 1000, min: 50 }, { size: '80%', max: 2000 }] });
	$('#splitter').on('resize', function (event) {
		resizeMap();
	});
	$('#splitter').on('expanded', function (event) {
		resizeMap();
	});
	$('#splitter').on('collapsed', function (event) {
		resizeMap();
	});
});
