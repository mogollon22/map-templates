app = {};

require([
    "esri/map",
    "dojo/domReady!"
],

function(Map) {
    app.map = new Map("map", {
        basemap: "gray",
        //UA
        center: [-110.950331, 32.231504],
        zoom: 16,

        //AZ
        //center: [-112.5511, 34.1334],
        //zoom: 7,

        //US
        //center: [-97, 37.5],
        //zoom: 5,
        attributionWidth: 0,
        logo: false,
        smartNavigation: false
    });


    //start coding here...
});